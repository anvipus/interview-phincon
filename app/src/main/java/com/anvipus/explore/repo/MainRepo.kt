package com.anvipus.explore.repo

import androidx.lifecycle.LiveData
import com.anvipus.explore.api.AuthApi
import com.anvipus.library.api.ApiCall
import com.anvipus.library.model.Pokemon
import com.anvipus.library.model.PokemonDetailData
import com.anvipus.library.model.PokemonListData
import retrofit2.Call
import com.anvipus.library.model.Resource
import javax.inject.Inject

class MainRepo @Inject constructor(
    private val api: AuthApi
){
    fun getListPokemon(): LiveData<Resource<PokemonListData>> = object : ApiCall<PokemonListData, PokemonListData>(){
        override fun createCall(): Call<PokemonListData> {
            val bodyParam = HashMap<String, String>()
            bodyParam["limit"] = "50"
            bodyParam["offset"] = "0"
            return api.getListPokemon(bodyParam)
        }
    }.asLiveData()

    fun getPokemonDetail(url: String): LiveData<Resource<PokemonDetailData>> = object : ApiCall<PokemonDetailData, PokemonDetailData>(){
        override fun createCall(): Call<PokemonDetailData> {
            return api.getPokemonDetail(url = url)
        }
    }.asLiveData()
}