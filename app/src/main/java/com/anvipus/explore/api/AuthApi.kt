package com.anvipus.explore.api

import com.anvipus.library.model.Pokemon
import com.anvipus.library.model.PokemonDetailData
import com.anvipus.library.model.PokemonListData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface AuthApi {
    @GET("pokemon")
    fun getListPokemon(
        @QueryMap params: Map<String, String>
    ): Call<PokemonListData>

    @GET
    fun getPokemonDetail(
        @Url url: String
    ): Call<PokemonDetailData>
}