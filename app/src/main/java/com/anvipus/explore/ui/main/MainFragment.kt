package com.anvipus.explore.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.anvipus.explore.R
import com.anvipus.explore.base.BaseFragment
import com.anvipus.explore.databinding.MainFragmentBinding
import com.anvipus.explore.utils.linear
import com.anvipus.library.model.Status
import com.anvipus.library.util.Constants
import com.anvipus.library.util.state.AccountManager
import com.bumptech.glide.Glide
import com.codedisruptors.dabestofme.di.Injectable
import com.google.gson.Gson
import javax.inject.Inject

class MainFragment : BaseFragment(), Injectable {

    companion object {
        fun newInstance() = MainFragment()
    }

    override val layoutResource: Int
        get() = R.layout.main_fragment

    override val statusBarColor: Int
        get() = R.color.colorAccent


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModelMain: MainViewModel by activityViewModels { viewModelFactory }
    private lateinit var binding: MainFragmentBinding

    @Inject
    lateinit var am: AccountManager

    private val mAdapter = PokemonListAdapter{ data ->
        getPokemonDetail(data.url!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ownTitle("List Pokemon")

        ownIcon(null)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MainFragmentBinding.bind(view)
        initView()
        initViewAction()
    }

    private fun initView(){
        with(binding){
            with(viewModelMain){
                with(rvMain){
                    linear()
                    adapter = mAdapter
                }
                getListPokemon().observe(viewLifecycleOwner){
                    showProgress(isShown = it?.status == Status.LOADING, isCancelable = false)
                    if(it?.status == Status.SUCCESS) {
                        mAdapter.submitList(it.data!!.results)
                    }
                }
            }
        }
    }

    private fun initViewAction(){

    }
    private fun getPokemonDetail(url : String){
        with(binding) {
            with(viewModelMain) {
                getPokemonDetail(url).observe(viewLifecycleOwner){
                    showProgress(isShown = it?.status == Status.LOADING, isCancelable = false)
                    if(it?.status == Status.SUCCESS) {
                        navigate(MainFragmentDirections.actionToDetail(it.data!!))
                    }
                    if(it?.status == Status.ERROR) {
                        Toast.makeText(requireContext(),it.msg,Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}