package com.anvipus.explore.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anvipus.explore.R
import com.anvipus.explore.databinding.ItemPokemonBinding
import com.anvipus.library.model.Pokemon

class PokemonListAdapter (
    private val itemClickCallback: (Pokemon) -> Unit
): ListAdapter<Pokemon, RecyclerView.ViewHolder>(COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(ItemPokemonBinding.bind(view))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        (holder as ViewHolder).run {
            bind(data)
            itemView.setOnClickListener {
                itemClickCallback.invoke(data)
            }
        }
    }
    class ViewHolder(private val binding: ItemPokemonBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: Pokemon?){
            binding.data = data
        }
    }

    companion object{
        const val TAG:String = "PokemonListAdapter"
        val COMPARATOR = object : DiffUtil.ItemCallback<Pokemon>(){
            override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
                return oldItem.name == newItem.name
            }

        }
    }
}