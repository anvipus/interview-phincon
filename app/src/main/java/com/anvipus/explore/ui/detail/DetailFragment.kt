package com.anvipus.explore.ui.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.anvipus.explore.R
import com.anvipus.explore.base.BaseFragment
import com.anvipus.explore.databinding.DetailFragmentBinding
import com.anvipus.explore.ui.main.MainViewModel
import com.anvipus.explore.ui.main.PokemonListAdapter
import com.anvipus.explore.utils.linear
import com.anvipus.library.util.state.AccountManager
import com.bumptech.glide.Glide
import com.codedisruptors.dabestofme.di.Injectable
import javax.inject.Inject

class DetailFragment : BaseFragment(), Injectable {

    companion object {
        fun newInstance() = DetailFragment()
    }

    override val layoutResource: Int
        get() = R.layout.detail_fragment

    override val statusBarColor: Int
        get() = R.color.colorAccent


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModelMain: MainViewModel by activityViewModels { viewModelFactory }
    private lateinit var binding: DetailFragmentBinding

    private val params by navArgs<DetailFragmentArgs>()

    @Inject
    lateinit var am: AccountManager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ownTitle("Detail Pokemon")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DetailFragmentBinding.bind(view)
        initView()
        initViewAction()
    }

    private fun initView(){
        with(binding){
            with(viewModelMain){
                data = params.data
            }
        }
    }

    private fun initViewAction(){

    }

}