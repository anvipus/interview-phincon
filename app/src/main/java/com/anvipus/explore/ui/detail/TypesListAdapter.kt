package com.anvipus.explore.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anvipus.explore.R
import com.anvipus.explore.databinding.ItemPokemonBinding
import com.anvipus.explore.databinding.ItemTypesBinding
import com.anvipus.library.model.Pokemon
import com.anvipus.library.model.Types

class TypesListAdapter (
    private val itemClickCallback: (Types) -> Unit
): ListAdapter<Types, RecyclerView.ViewHolder>(COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_types, parent, false)
        return ViewHolder(ItemTypesBinding.bind(view))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        (holder as ViewHolder).run {
            bind(data)
            itemView.setOnClickListener {
                itemClickCallback.invoke(data)
            }
        }
    }
    class ViewHolder(private val binding: ItemTypesBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: Types?){
            binding.data = data
        }
    }

    companion object{
        const val TAG:String = "TypesListAdapter"
        val COMPARATOR = object : DiffUtil.ItemCallback<Types>(){
            override fun areItemsTheSame(oldItem: Types, newItem: Types): Boolean {
                return oldItem.type?.name == oldItem.type?.name
            }

            override fun areContentsTheSame(oldItem: Types, newItem: Types): Boolean {
                return oldItem.type?.name == oldItem.type?.name
            }

        }
    }
}