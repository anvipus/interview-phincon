package com.anvipus.library.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
class PokemonListData (
    @Json(name = "results")
    val results: List<Pokemon>?
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class PokemonDetailData (
    @Json(name = "abilities")
    val abilities: List<Abilities>?,
    @Json(name = "base_experience")
    val base_experience: Int?,
    @Json(name = "cries")
    val cries: Cries?,
    @Json(name = "forms")
    val forms: List<Forms>?,
    @Json(name = "height")
    val height: String?,
    @Json(name = "id")
    val id: String?,
    @Json(name = "moves")
    val moves: List<Moves>?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "stats")
    val stats: List<Stats>?,
    @Json(name = "types")
    val types: List<Types>?,
    @Json(name = "weight")
    val weight: String?,
) : Parcelable {

    val pokemonId get() = "Pokemon Id : $id"
    val pokemonName get() = "Pokemon Name : $name"
    val pokemonWeight get() = "Pokemon Weight : $weight lbs"
    val pokemonHeight get() = "Pokemon Height : $height lbs"
    val pokemonBaseExperience get() = "Pokemon Base Experienced : $base_experience"

    val pokemonTypes get() = "Pokemon Types : ${getType(types!!)}"

    val pokemonMoves get() = "Pokemon Moves : ${getMoves(moves!!)}"

    val allString get() = "$pokemonId \n$pokemonName \n$pokemonWeight \n$pokemonHeight \n$pokemonBaseExperience \n$pokemonTypes \n$pokemonMoves"
    private fun getType(listData: List<Types>): String?{
        var type: String? = ""

        for(data in listData){
            if(type.isNullOrEmpty()){
                type = "${data.type?.name}"
            }else{
                type = "$type , ${data.type?.name}"
            }

        }
        return type
    }

    private fun getMoves(listData: List<Moves>): String?{
        var type: String? = ""

        for(data in listData){
            if(type.isNullOrEmpty()){
                type = "${data.move?.name}"
            }else{
                type = "$type , ${data.move?.name}"
            }

        }
        return type
    }

}