package com.anvipus.library.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
class Pokemon (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable {
    val pokemonName get() = "Pokemon Name : $name"
}

@JsonClass(generateAdapter = true)
@Parcelize
class Abilities (
    @Json(name = "is_hidden")
    val is_hidden: Boolean?,
    @Json(name = "slot")
    val slot: Int?,
    @Json(name = "ability")
    val ability: Ability?
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Ability (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Cries (
    @Json(name = "latest")
    val latest: String?,
    @Json(name = "legacy")
    val legacy: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Forms (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Moves (
    @Json(name = "move")
    val move: Move?,
    @Json(name = "url")
    val url: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Move (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Stats (
    @Json(name = "base_stat")
    val base_stat: String?,
    @Json(name = "effort")
    val effort: String?,
    @Json(name = "stat")
    val stat: Stat?
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Stat (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
class Types (
    @Json(name = "slot")
    val slot: String?,
    @Json(name = "type")
    val type: Type?
) : Parcelable{
    val pokemonType get() = "Pokemon Type : ${type?.name}"
}

@JsonClass(generateAdapter = true)
@Parcelize
class Type (
    @Json(name = "name")
    val name: String?,
    @Json(name = "url")
    val url: String?,
) : Parcelable
